#!/usr/bin/env python
# Copyright (c) 2020 Universidad de Costa Rica
#
# Author: Federico Ruiz-Ugalde <memeruiz@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from python_robot_misc_utils import joint_sim, control


class My_cl(control.control_loop.Controlloop):
    def set_params(self, params):
        self.joints = joints
        self.counter = 0
        self.joints.init_time(self.next_time)

    def process(self):
        # q = self.joints.get_q(self.next_time)
        # print("q: ", q * 180.0 / pi)
        self.joints.update(self.next_time)
        if self.counter == 10:
            print("Setting new motor torque")
            self.joints.set_motor_torque([-10.])
        if self.counter > 20:
            self.joints.set_motor_torque([0.])
            print("Setting new position")
            # self.joints.set_q_ref(array([10.0 * pi / 180.0]))
            self.counter = 0
        else:
            self.counter += 1


if __name__ == "__main__":
    print("Joint sim example")
    # joints = joint_sim.Sim_joint_impedance_inertia(
    #    [0.5], [0.1], [0.1], [0.1], 1,
    #    [[-180.0 * pi / 180.0, 180.0 * pi / 180.0]])

    joints = joint_sim.Sim_mech_joints(num_joints=1,
                                       inertia=[1.0],
                                       static_friction=[1.0],
                                       viscous_friction_coef=[1.0])

    cl = My_cl(30)
    cl.loop(joints=joints)
