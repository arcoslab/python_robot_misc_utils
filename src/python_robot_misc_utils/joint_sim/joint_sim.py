# Copyright (c) 2009 Technische Universitaet Muenchen, Informatik
# Lehrstuhl IX.
# Author: Federico Ruiz-Ugalde <memeruiz@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from numpy import array, pi, maximum, minimum
import numpy
import logging
from time import time as ttime
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:joint_sim: %(message)s')
ch.setFormatter(formatter)
# logger.handlers.clear()
logger.addHandler(ch)

numpy.seterr(divide='raise')

try:
    range = xrange
except NameError:
    pass


def float_printer(a):
    return ("{: 6.2f}".format(a))


def float_printer4(a):
    return ("{: 6.4f}".format(a))


class Controller_type(object):
    position = 0
    velocity = 1


class Sim_controllers(object):
    """
    Simulate controllers for arrays of joints

    Takes joint current state, position or velocity references and
    produces motor torques
    """

    def __init__(self,
                 num_joints=1,
                 kqp=[0.1],
                 kqi=[0.0],
                 kqd=[0.0],
                 kqvp=[0.1],
                 kqvi=[0.0],
                 stiffness=[0.1],
                 ctrl_type=Controller_type.position,
                 ctrl_joint_limits=[[
                     -170.0 * pi / 170.0,
                     170.0 * pi / 170.0,
                 ]],
                 q_ref_delta_max=[5.0 * pi / 180.0],
                 q_ke_i_lim=[0.1],
                 torque_ref_max=[10.0],
                 viscous_friction_coef=[1.0]):
        """
        Constructs a Simulated array of joint controllers

        num_joints: Number of joints
        kqp: p constant for pi position controller
        kqi: i constant for pi position controller
        kqd: d constant for pid position controller
        kqvp: p constant for pi velocity controller (Not impl.)
        kqvi: i constant for pi velocity controller (Not impl.)
        stiffness: Stiffness for internal impedance controller
        ctrl_type: Select controller type (position/velocity) (Not used)
        ctrl_joint_limits: Controller joint limits (controller avoids
                           movement outside these limits)
        q_ref_delta_max: Maximum allowed reference step change (rads)
                         (Not impl.)
        q_ke_i_lim: Limit for i pid component
        torque_ref_max: Maximum joint torque (total)
        viscous_friction_coef: angular viscous friction coef. (N m s)
        """
        self.num_joints = num_joints
        self.kqp = array(kqp)
        self.kqi = array(kqi)
        self.kqd = array(kqd)
        self.kqvp = array(kqvp)
        self.kqvi = array(kqvi)
        self.stiffness = array(stiffness)
        self.ctrl_type = ctrl_type
        self.ctrl_joint_limits = array(ctrl_joint_limits)
        self.q_ref_delta_max = array(q_ref_delta_max)
        self.q_ke_i_lim = array(q_ke_i_lim)
        self.torque_ref_max = array(torque_ref_max)
        self.viscous_friction_coef = array(viscous_friction_coef)

        self.q_ke = array([0.0] * self.num_joints)
        self.q_ke_i = array([0.0] * self.num_joints)
        self.qve = array([0.0] * self.num_joints)
        self.qvei = array([0.0] * self.num_joints)

        self.q_ref = array([0.0] * self.num_joints)
        self.qv_ref = array([0.0] * self.num_joints)
        # TODO: add max_speed param

    def set_q_ref(self, q_ref):
        self.q_ref = array(q_ref)
        pass

    def get_q_ref(self):
        return(self.q_ref)

    def set_qv_ref(self, qv_ref):
        self.qv_ref = array(qv_ref)
        pass

    def set_stiffness(self, stiffness):
        self.stiffness = stiffness

    def set_ctrl_type(self, ctrl_type):
        # TODO clean controller state variables before switching
        self.ctrl_type = ctrl_type

    def update(self, q, qv, measured_ext_torque, delta_time):
        # update joint state
        # print("Update Controller")

        # numpy.set_printoptions(formatter={'float': float_printer4},
        #                       suppress=True)
        #logger.debug("ctrl: q_ref-q: " + str((self.q_ref - q) * 180.0 / pi))

        # Impedance equation: t_react=K(qref-q*) is converted to actually
        # control q*. The idea is to send the joint in position control
        # to q* in order to generate the impedance equation.
        # In the static case t_react == t_ext
        # We measure t_ext, the force is actually being controlled by
        # the external entity. With the robot the only thing we can control
        # is the position to generate an emulated torsional spring
        # q*=q_ref - t_react/K, we want to produce q* with the motor
        # q_kref == q*
        # t_react is exactly opposite to t_external (is what we want the
        # outside load to feel about the joint)

        self.torque_react = -measured_ext_torque
        self.q_kref = self.q_ref - self.torque_react / self.stiffness

        # print(f"Torque react: {self.torque_react}")
        # print(f"Q ref: {self.q_ref}")
        # print(f"Stiff: {self.stiffness}")
        # print(f"q_kref: {self.q_kref}")

        self.old_q_ke = self.q_ke
        self.q_ke = self.q_kref - q
        # logger.debug((f"ctrl: Q_kref: {self.q_kref*180.0/pi}, "
        #              f"q_ke: {self.q_ke*180.0/pi}"))

        self.q_ke_i += self.q_ke
        self.q_ke_i = minimum(self.q_ke_i, self.q_ke_i_lim)
        self.q_ke_i = maximum(self.q_ke_i, -self.q_ke_i_lim)
        self.delta_q_ke = self.q_ke - self.old_q_ke
        self.p = self.q_ke * self.kqp
        self.i = self.q_ke_i * self.kqi
        self.d = (self.delta_q_ke / delta_time) * self.kqd
        self.torque_ref = self.p + self.i + self.d
        # controls the maximum acceleration
        self.torque_ref = minimum(self.torque_ref, self.torque_ref_max)
        self.torque_ref = maximum(self.torque_ref, -self.torque_ref_max)

        # We desire that the joint moves according to total
        # equivalent torque: self.torque_ref, therefore
        # this is the variable we want to control
        # The applied motor torque is the final desired torque
        # minus the already applied measured ext torque and
        # the friction component. This two, uncontrollable forces,
        # may be helping or fighting what we desired
        self.motor_torque = self.torque_ref - \
            measured_ext_torque - self.viscous_friction_coef*qv

        # numpy.set_printoptions(formatter={'float': float_printer4},
        #                        suppress=True)
        # print_msg = (f'ctrl: Ctrl_type: {self.ctrl_type}, '
        #              f'Q_ref: {self.q_ref * 180.0 / pi}, '
        #              f'q: {q * 180.0 / pi}, qv: {qv * 180.0 / pi}, '
        #              f'qe: {((self.q_ref - q) * 180.0 / pi)}, '
        #              f'q_ke: {(self.q_ke* 180.0 / pi)}, ')
        # logger.debug(print_msg)
        # numpy.set_printoptions(formatter={'float': float_printer4},
        #                        suppress=True)
        # print_msg = (f"ctrl: q_ke p: {self.p}, q_ke i: {self.i}, "
        #              f"q_ke d: {self.d}, "
        #              f"torque ref: {self.torque_ref}, "
        #              f"viscous_torque: {-self.viscous_friction_coef*qv}, "
        #              f"motor_torque: {self.motor_torque}")
        # logger.debug(print_msg)


class Sim_mech_joints(object):
    """
    Simulate the mechanics of an array of joints

    Given a external and motor torques, it updates the current
    state variables q, qv, qa"""

    def __init__(self,
                 name="My joint sim",
                 num_joints=1,
                 inertia=[1.0],
                 static_friction=[1.0],
                 viscous_friction_coef=[1.0],
                 hard_joint_limits=[[
                     -180.0 * pi / 180.0,
                     180.0 * pi / 180.0,
                 ]],
                 limit=True):
        """
        Constructs a Simulated array of joints

        num_joints: Number of joints
        inertia: Angular mass (kg m^2)
        static_friction: angular static friction (N m) (not used yet)
        viscous_friction_coef: angular viscous friction coef. (N m s)
        hard_joint_limits: mechanical joint limits
        """
        self.num_joints = num_joints
        self.inertia = array(inertia)
        self.static_friction = array(static_friction)
        self.viscous_friction_coef = array(viscous_friction_coef)
        self.hard_joint_limits = array(hard_joint_limits)
        self.started = False
        self.q = array([0.0] * self.num_joints)  # angular postion
        self.qv = array([0.0] * self.num_joints)  # angular velocity
        self.qa = array([0.0] * self.num_joints)  # angular acceleration
        self.motor_torque = array([0.0] * self.num_joints)
        self.torque_ext = array([0.0] * self.num_joints)
        self.torque_int = array([0.0] * self.num_joints)
        self.limit = limit

    def set_q(self, q):
        """Force current angular postion"""
        self.q = array(q)

    def set_qv(self, qv):
        """Force current angular velocity"""
        self.qv = array(qv)

    def set_qa(self, qa):
        """Force current angular acceleration"""
        self.qa = array(qa)

    def set_motor_torque(self, motor_torque):
        """Set motor torque"""
        self.motor_torque = array(motor_torque)

    def init_time(self, time):
        """Set initial time"""
        self.time = time
        self.started = True

    def limit_joints(self):
        #q_tmp = array(self.q)
        # checking low limit
        # print("Limit joints")
        # print(self.hard_joint_limits)
        # print(self.hard_joint_limits[:, 0])
        # print(self.hard_joint_limits[:, 1])
        # print("Q")
        # print(self.q)
        q_tmp = maximum(self.hard_joint_limits[:, 0], self.q)
        # cheking high limits
        q_tmp = minimum(self.hard_joint_limits[:, 1], q_tmp)
        for n, i in enumerate((q_tmp - self.q) == array([0.0] *
                                                        self.num_joints)):
            if not i:
                print("Joint: ", n, " saturated")
                self.qv[n] = 0.0
                self.qa[n] = 0.0
        # if (q_tmp - self.q).any():
            # print("Limiting: ", (q_tmp-q)*180.0/pi)

        #    pass
        self.q = q_tmp

    def update(self, time):
        """Execute state update"""
        # print()
        # print("Mech update")
        self.t_start = ttime()
        # import pdb
        # pdb.set_trace()
        if not self.started:
            raise RuntimeError("time not yet initialized")
        else:
            new_time = time
            self.delta_time = new_time - self.time
            self.time = new_time

            # Torque calculation
            # (TODO: consider static friction)
            viscous_torque = -self.viscous_friction_coef * self.qv
            self.torque_int = self.motor_torque + viscous_torque

            self.torque = self.torque_ext + self.torque_int
            # numpy.set_printoptions(formatter={'float': float_printer4},
            #                        suppress=True)
            # logger.debug((f"sim: Torque: {self.torque} = "
            #               f"Torque_ext: {self.torque_ext} + "
            #               f"torque_int: {self.torque_int} "
            #               f"Motor torque: {self.motor_torque} + "
            #               f"Viscous_torque: {viscous_torque}"))
            # print((f"sim: Torque: {self.torque} = "
            #        f"Torque_ext: {self.torque_ext} + "
            #        f"torque_int: {self.torque_int} "
            #        f"Motor torque: {self.motor_torque} + "
            #        f"Viscous_torque: {viscous_torque}"))

            # print(f"Delta_time: {self.delta_time}")
            # print(f"qa: {self.qa}, qv: {self.qv}, q: {self.q}")
            # motion equations
            self.qa = self.torque / self.inertia
            self.qv = self.qv + self.qa * self.delta_time
            self.q = self.q + self.qv*self.delta_time + \
                self.qa*(self.delta_time**2)/2.0

            # print(f"qa: {self.qa}, qv: {self.qv}, q: {self.q}")

            # TODO: check for joint limits
            if self.limit:
                self.limit_joints()

            # print(f"qa: {self.qa}, qv: {self.qv}, q: {self.q}")
            # logger.debug((f"sim: qa: {self.qa * 180.0 / pi}, "
            #               f"qv: {self.qv * 180.0 / pi}, "
            #               f"q: {self.q * 180.0 / pi}"))

    def get_q(self):
        return (self.q)

    def get_qv(self):
        return (self.qv)

    def get_measured_torque_ext(self):
        """This returns the measured torque produced by
        an external interaction with the environment"""
        # TODO emulate some sort of noise or typical
        # sensor error
        return (self.torque_ext)

    def set_torque_ext(self, torque_ext):
        """This is the external torque produced by the environment when
        the joint interacts with the environment.
        This must be set by an external environment emulator or
        something similar
        """
        self.torque_ext = array(torque_ext)
