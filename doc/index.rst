.. Python Robot Misc Utilities documentation master file, created by
   sphinx-quickstart on Fri May 28 17:11:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python Robot Misc Utilities's documentation!
=======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
