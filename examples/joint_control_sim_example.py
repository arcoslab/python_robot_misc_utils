#!/usr/bin/env python
# Copyright (c) 2020 Universidad de Costa Rica
#
# Author: Federico Ruiz-Ugalde <memeruiz@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from python_robot_misc_utils import joint_sim, control
from numpy import pi, array


class My_cl(control.control_loop.Controlloop):
    def set_params(self, params):
        self.joints = joints
        self.counter = 0
        self.joints.init_time(self.next_time)
        self.joint_controllers = joint_controllers
        self.measured_ext_torque = array([0.0])
        self.my_force = 0.0

    def process(self):
        if self.localtime <= 4:
            self.joint_controllers.set_q_ref(array([00.0 * pi / 180.0]))
        elif self.localtime < 10:
            # print("Setting new motor torque")
            # self.joints.set_motor_torque([-10.])
            self.joint_controllers.set_q_ref(array([100.0 * pi / 180.0]))
        elif (self.localtime < 30):
            # self.joints.set_motor_torque([0.])
            self.my_force -= 0.0001
            if self.my_force <= -0.1:
                self.my_force = -0.1
            # self.my_force = -0.1
            if self.localtime > 20:
                self.my_force += 0.0001
            print("My force: ", self.my_force)
            self.joints.set_torque_ext([self.my_force])
            # print("Setting new position")
            # self.joints.set_q_ref(array([10.0 * pi / 180.0]))
        elif (self.localtime < 40):
            self.joints.set_torque_ext([0.0])
        elif (self.localtime < 50):
            self.joints.set_torque_ext([-0.02])

        # q = self.joints.get_q(self.next_time)
        # print("q: ", q * 180.0 / pi)
        print("time: %6.2f " % (self.localtime))
        self.joints.update(self.next_time)
        self.measured_ext_torque = self.joints.get_measured_torque_ext()
        self.joint_controllers.update(
            q=self.joints.get_q(),
            qv=self.joints.get_qv(),
            measured_ext_torque=self.measured_ext_torque,
            delta_time=self.delta_time)
        self.joints.set_motor_torque(self.joint_controllers.motor_torque)


if __name__ == "__main__":
    print("Joint sim example")
    # joints = joint_sim.Sim_joint_impedance_inertia(
    #    [0.5], [0.1], [0.1], [0.1], 1,
    #    [[-180.0 * pi / 180.0, 180.0 * pi / 180.0]])
    viscous_friction_coef = 0.5

    joints = joint_sim.Sim_mech_joints(
        num_joints=1,
        inertia=[1.0],
        viscous_friction_coef=[viscous_friction_coef])

    joint_controllers = joint_sim.Sim_controllers(
        num_joints=1,
        kqp=[35.0],
        kqi=[0.1],
        kqd=[16.0],
        stiffness=[0.1],
        ctrl_joint_limits=[[
            -170.0 * pi / 170.0,
            170.0 * pi / 170.0,
        ]],
        q_ke_i_lim=[0.01],
        torque_ref_max=[10.0],
        viscous_friction_coef=[viscous_friction_coef])

    cl = My_cl(20)
    cl.loop(joints=joints, joint_controllers=joint_controllers)
