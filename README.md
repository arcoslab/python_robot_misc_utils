[![pipeline status](https://gitlab.com/arcoslab/python_robot_misc_utils/badges/master/pipeline.svg)](https://gitlab.com/arcoslab/python_robot_misc_utils/-/commits/master)

[![coverage report](https://gitlab.com/arcoslab/python_robot_misc_utils/badges/master/coverage.svg)](https://gitlab.com/arcoslab/python_robot_misc_utils/-/commits/master)

# python_robot_misc_utils

Miscellaneous python helper function and classes

*  pmanager : Process manager class
*  control_loop: Control loop class
*  nanosleep: Nanosecond sleep function
*  dprint: debug printing
*  myopen: wrapped popen
*  rawkey: raw keyboard capture
*  joint_sim: A simple mechanical and controller joint impedance simulator

## Installation notes

It is recommended to use the following command if you are going to be developing in this package:

pip3 install -e .
