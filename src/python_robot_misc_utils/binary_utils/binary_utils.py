# -*- coding: utf-8 -*-
# Copyright (c) 2013 Federico Ruiz-Ugalde
# Author: Federico Ruiz Ugalde <memeruiz at gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from functools import reduce
import ctypes


def binlist_to_string(binlist):
    """ Returns a string from a list of binary data

    It interprets the binary items as ascii characters
    """
    return(reduce(lambda x, y: x+y, map(chr, binlist)))


def flag_state(reg_data, flag_pos):
    """ Function that calculates the state of a bit flag from binary

    reg_data: binary register data
    flag_pos: position of the flag of interest
    """
    return((((0x0001 << flag_pos) & reg_data) > 0))


class floating_point_interpreter(object):
    """ Class for interpreting binary data as floating point numbers

    It takes binary data (possibly multibyte data) and interprets it
    as a floating point number. It can take a variable number of bits
    for the exponential part of the number and the rest for the
    significand or mantissa part. Each part (mantissa and exponential)
    are treated as twos-complement numbers.

    Y: Mantissa
    N: Exponential

    O = Y * 2^N

    Attributes
    ----------
    N: int (TODO)
       Exponential
    Y: int (TODO)
       Mantissa

    Methods
    -------
    set_n_bits(n_bits=5):
       sets the exponential bits quantity

    bin_input(bindata):
       sets the input data

    get_n(), get_y():
       get internal exponential and mantissa parts

    calc_linear_value(bindata):
       calculates and returns the floating point number for a
       particular bindata input, according to the currently set
       number of bits for the exponential
    """

    def __init__(self, n_bits=5):
        self.set_n_bits(n_bits)

    def set_n_bits(self, n_bits):
        self.n_bits = n_bits
        self._my_fields = [
            ("Y", ctypes.c_int16, 16-n_bits),
            ("N", ctypes.c_int16, n_bits),
        ]
        self.my_struct_class = type('floating_point_data_format',
                                    (ctypes.LittleEndianStructure,),
                                    {"_fields_": self._my_fields})
        self.my_union_class = type('floating_point_data_union',
                                   (ctypes.Union,),
                                   {
                                       "_anonymous_": ("bit",),
                                       "_fields_": [
                                           ("bit", self.my_struct_class),
                                           ("asByte", ctypes.c_uint16)
                                       ]
                                   }
                                   )
        self.converter = self.my_union_class()

    def bin_input(self, bindata):
        self.converter.asByte = bindata

    def get_n(self):
        return(self.converter.N)

    def get_y(self):
        return(self.converter.Y)

    def calc_fp_value(self, bindata):
        self.bin_input(bindata)
        return(self.converter.Y * 2**self.converter.N)
