#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import print_function
from python_robot_misc_utils.rawkey import Raw_key, Keys, is_key
from time import sleep
import sys


def main():
    raw_key = Raw_key()
    while True:
        sleep(0.02)
        print('new cycle', end=' ')
        num_chars = raw_key.get_num_chars(timeout=0.1)
        print('got ', num_chars, end=' ')
        if is_key(num_chars, Keys.RIGHT_ARROW):
            print('Right arrow', end='')
        if is_key(num_chars, Keys.d):
            print('d', end='')
        print(' '*40, end='')
        print('', end='\r')
        sys.stdout.flush()


if __name__ == '__main__':
    main()
